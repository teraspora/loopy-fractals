# Loopy Fractals website

## January 2019

I am building this initially to practise using the Materialize CSS framework.

All the images, videos and shaders and code are my own work, but the initial code is based on a great set of tutorials I found at https://www.youtube.com/playlist?list=PL4cUxeGkcC9gGrbtvASEZSlFEYBnPkmff.

I shall be experimenting with different Materialize components.

Set git remote to https://gitlab.com/teraspora/loopy-fractals, added a `.gitlab-ci.yml` file